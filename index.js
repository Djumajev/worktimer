const { app, BrowserWindow } = require('electron');

function createWindow () {
    let win = new BrowserWindow({
        width: 800,
        height: 600,
        frame: false,
        fullscreen: true
    });

    win.hide();
    win.loadFile('index.html');

    loop(win);
}

app.on('ready', createWindow);

var start_t = getTime();

const work_t = 2 * 60 * 60 * 1000; //2  hours
const rest_t = 15 * 60 * 1000; //15 minutes

var rest = true;
var started = false;

function loop(win) {

    if (rest) {
        if (!started) {
            win.show();
            start_t = getTime();
            started = !started;
        }

        if (getTime() - start_t >= rest_t) {
            started = false;
            rest = false;
        } else {
            win.webContents.send('time', start_t + rest_t - getTime()); 
        }
    } else {
        if (!started) {
            win.hide();
            start_t = getTime();
            started = !started;
        }

        if (getTime() - start_t >= work_t) {
            started = false;
            rest = true;
        } else {
            win.webContents.send('time', start_t + rest_t - getTime()); 
        }
    }

    setTimeout(loop, 500, win);
}

function getTime() {
    let d = new Date();
    return d.getTime();
}