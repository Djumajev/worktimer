var scene = new engine();

const vw = scene.width / 100;
const vh = scene.height / 100;

var txt = '';
require('electron').ipcRenderer.on('time', (event, message) => {
    txt = message;
});

scene.update = function () {
    new scene.fillTextLine({
        text: SecondsTohhmm(txt/1000),
        x: 50 * vw,
        y: 50 * vh,
        align: "center",
        color: "#FFF",
        font: 15 * vh + "px Arial"
    }).draw();
}

var SecondsTohhmm = function(totalSeconds) {
    var hours   = Math.floor(totalSeconds / 3600);
    var minutes = Math.floor((totalSeconds - (hours * 3600)) / 60);
    var seconds = totalSeconds - (hours * 3600) - (minutes * 60);
  
    seconds = Math.floor(seconds);
      
    var result = (hours < 10 ? "0" + hours : hours);
    result += ":" + (minutes < 10 ? "0" + minutes : minutes);
    result += ":" + (seconds < 10 ? "0" + seconds : seconds);
    return result;
  }